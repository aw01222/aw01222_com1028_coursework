package aw01222_COM1028_Coursework;

import java.sql.*;

public class BaseQuery {
	
	static Connection connection = null;
	static String databaseName = "classicmodels";
	static String url = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String username = "root";
	static String password = "Silverstar98";

	public static void main(String[] args) {
		
		try {
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
			
			connection = DriverManager.getConnection(url, username, password);
			
			Statement myStmt = connection.createStatement();
			
			ResultSet myRs = myStmt.executeQuery("select * from employees");
			
			while (myRs.next()) {
				System.out.println(myRs.getString("lastName") + ", " + myRs.getString("firstName"));
			}
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
			
		}
		

	}

}
