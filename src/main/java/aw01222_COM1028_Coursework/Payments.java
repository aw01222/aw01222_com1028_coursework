package aw01222_COM1028_Coursework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Payments {
	
	static Connection connection = null;
	static String databaseName = "classicmodels";
	static String url = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String username = "root";
	static String password = "Silverstar98";

	public static void main(String[] args) {
		
		try {
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
			
			connection = DriverManager.getConnection(url, username, password);
			
			Statement myStmt = connection.createStatement();
			
			ResultSet myRs = myStmt.executeQuery("select * from payments order by paymentDate");
			
			while (myRs.next()) {
				System.out.println("On the " + myRs.getString("paymentDate") + " the total payments are: $" + myRs.getString("amount"));
			}
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
			
		}

	}

}
