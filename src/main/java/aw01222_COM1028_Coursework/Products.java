package aw01222_COM1028_Coursework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Products {
	
	static Connection connection = null;
	static String databaseName = "classicmodels";
	static String url = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String username = "root";
	static String password = "Silverstar98";


	public static void main(String[] args) {
		
		try {
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
			
			connection = DriverManager.getConnection(url, username, password);
			
			Statement myStmt = connection.createStatement();
			
			Statement myStmt2 = connection.createStatement();
			
			Statement myStmt3 = connection.createStatement();	
			
			Statement myStmt4 = connection.createStatement();
			
			Statement myStmt5 = connection.createStatement();
			
			Statement myStmt6 = connection.createStatement();
			
			Statement myStmt7 = connection.createStatement();
			
			ResultSet myRs = myStmt.executeQuery("select * from products WHERE productLine = 'Classic Cars'");
			
			ResultSet myRs2 = myStmt2.executeQuery("select * from products WHERE productLine = 'Motorcycles'");
			
			ResultSet myRs3 = myStmt3.executeQuery("select * from products WHERE productLine = 'Planes'");
			
			ResultSet myRs4 = myStmt4.executeQuery("select * from products WHERE productLine = 'Ships'");
			
			ResultSet myRs5 = myStmt5.executeQuery("select * from products WHERE productLine = 'Trains'");
			
			ResultSet myRs6 = myStmt6.executeQuery("select * from products WHERE productLine = 'Trucks and Buses'");
			
			ResultSet myRs7 = myStmt7.executeQuery("select * from products WHERE productLine = 'Vintage Cars'");
			
			
			System.out.println("\nClassic Cars includes the products: \n");
			while (myRs.next()) {
				System.out.println("Product Code: " + myRs.getString("productCode") + ", Product Name: " + myRs.getString("productName") + ", Product Scale: " + myRs.getString("productScale") + ", Product Vendor: " + myRs.getString("productVendor") + ", Product Description: " + myRs.getString("productDescription") + ", Quantity In Stock: " + myRs.getString("quantityInStock") + ", Buy Price: " + myRs.getString("buyPrice") + ", MSRP: " + myRs.getString("MSRP"));
			}
			System.out.println("\nMotorcycles includes the products: \n");
			while (myRs2.next()) {
				System.out.println("Product Code: " + myRs2.getString("productCode") + ", Product Name: " + myRs2.getString("productName") + ", Product Scale: " + myRs2.getString("productScale") + ", Product Vendor: " + myRs2.getString("productVendor") + ", Product Description: " + myRs2.getString("productDescription") + ", Quantity In Stock: " + myRs2.getString("quantityInStock") + ", Buy Price: " + myRs2.getString("buyPrice") + ", MSRP: " + myRs2.getString("MSRP"));
			}
			System.out.println("\nPlanes includes the products: \n");
			while (myRs3.next()) {
				System.out.println("Product Code: " + myRs3.getString("productCode") + ", Product Name: " + myRs3.getString("productName") + ", Product Scale: " + myRs3.getString("productScale") + ", Product Vendor: " + myRs3.getString("productVendor") + ", Product Description: " + myRs3.getString("productDescription") + ", Quantity In Stock: " + myRs3.getString("quantityInStock") + ", Buy Price: " + myRs3.getString("buyPrice") + ", MSRP: " + myRs3.getString("MSRP"));
			}
			System.out.println("\nShips includes the products: \n");
			while (myRs4.next()) {
				System.out.println("Product Code: " + myRs4.getString("productCode") + ", Product Name: " + myRs4.getString("productName") + ", Product Scale: " + myRs4.getString("productScale") + ", Product Vendor: " + myRs4.getString("productVendor") + ", Product Description: " + myRs4.getString("productDescription") + ", Quantity In Stock: " + myRs4.getString("quantityInStock") + ", Buy Price: " + myRs4.getString("buyPrice") + ", MSRP: " + myRs4.getString("MSRP"));
			}
			System.out.println("\nTrains includes the products: \n");
			while (myRs5.next()) {
				System.out.println("Product Code: " + myRs5.getString("productCode") + ", Product Name: " + myRs5.getString("productName") + ", Product Scale: " + myRs5.getString("productScale") + ", Product Vendor: " + myRs5.getString("productVendor") + ", Product Description: " + myRs5.getString("productDescription") + ", Quantity In Stock: " + myRs5.getString("quantityInStock") + ", Buy Price: " + myRs5.getString("buyPrice") + ", MSRP: " + myRs5.getString("MSRP"));
			}
			System.out.println("\nTrucks and Buses includes the products: \n");
			while (myRs6.next()) {
				System.out.println("Product Code: " + myRs6.getString("productCode") + ", Product Name: " + myRs6.getString("productName") + ", Product Scale: " + myRs6.getString("productScale") + ", Product Vendor: " + myRs6.getString("productVendor") + ", Product Description: " + myRs6.getString("productDescription") + ", Quantity In Stock: " + myRs6.getString("quantityInStock") + ", Buy Price: " + myRs6.getString("buyPrice") + ", MSRP: " + myRs6.getString("MSRP"));
			}
			System.out.println("\nVintage Cars includes the products: \n");
			while (myRs7.next()) {
				System.out.println("Product Code: " + myRs7.getString("productCode") + ", Product Name: " + myRs7.getString("productName") + ", Product Scale: " + myRs7.getString("productScale") + ", Product Vendor: " + myRs7.getString("productVendor") + ", Product Description: " + myRs7.getString("productDescription") + ", Quantity In Stock: " + myRs7.getString("quantityInStock") + ", Buy Price: " + myRs7.getString("buyPrice") + ", MSRP: " + myRs7.getString("MSRP"));
			}
			
		
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
			
		}

	}

}
