package aw01222_COM1028_Coursework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Orders {
	
	static Connection connection = null;
	static String databaseName = "classicmodels";
	static String url = "jdbc:mysql://localhost:3306/classicmodels?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	static String username = "root";
	static String password = "Silverstar98";

	public static void main(String[] args) {
		
		try {
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
			
			connection = DriverManager.getConnection(url, username, password);
			
			Statement myStmt = connection.createStatement();
			
			ResultSet myRs = myStmt.executeQuery("select orders.orderNumber, customers.customerName, sum(orderdetails.quantityOrdered*orderdetails.priceEach) AS Value from orders join customers on orders.customerNumber = customers.customerNumber join orderdetails on orders.orderNumber = orderdetails.orderNumber group by orderdetails.orderNumber having Value > 25000");
			
			while (myRs.next()) {
				System.out.println("Order Number: " + myRs.getString("orderNumber") + ", Customer Name: " + myRs.getString("customerName") + ", Value: $" + myRs.getString("Value"));
			}
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
			
		}

	}

}
